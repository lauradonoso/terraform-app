resource "aws_instance" "example" {
  count         = 2
  ami           = "ami-0817d428a6fb68645"
  instance_type = var.instance_type
  key_name      = "jenkins-app"

  tags = {
    Name = "Terraform-${count.index + 1}"
  }

}